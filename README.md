# Caffe
CLI for Caffe Style Projects.

# Introduction
Based on a `project.yml`, the CLI will interpret it and creates
the scaffolding tree directory and the respective files.

It's important to know the `Caffe Projects` not only have a 
specific scaffolding but also have a **Dependency Injector** powered
by **Uber FX**. And it pretends to work with *Separated Entities*.

So, the project configuration is for two things:
- Declaration of the Entities
- Dependency Relation

# Project Configuration
The yml file looks like this

`````yaml
project:
  name: ping-api
  application: 
    http:
      usage: "gin"
      swagger: "swaggo" 
      port: "${PORT}"
      routes:
        Ping:
          method: "GET"
          path: "/ping"
          handler: "#PingHandler"
        GetInfo:
          method: "GET"
          path: "/info/:info-id"
          handler: "@InfoController"
      handlers:
        PingHandler:
          api-doc: >
            @Summary Ping
            @Description Pings to the service expecting returning a 200 with "pong" response.
            @Tags Ping
            @Produce json
            @Success 200
            @Failed 502
            @Router /ping [get]
  entities:
    Info:
      domain:
        models:
          InfoSearch:
            InfoId: string
          InfoItem:
            Id: string
            Content: string
            Size: int
        interfaces:
            InfoRepository:
              Get:
                docs: "Retrieves an info item by its id"
                params: 
                  - "*InfoSearch"
                returns: 
                  - "*InfoItem"
        errors:
          ErrNotFound: "The info could not be found"
      repos:
        MongoInfoRepository:
          dependencies:
            logger: logger.Logger
            db: mongo.Client
          implements: ?InfoRepository
          inside-of: mongo
          inject:
            l: logger
            m: mongoClient
          construct:
            logger: l
            db: m
      services:
        GetInfoService:
          dependencies:
            logger: logger.Logger
            repo: ?InfoRepository
          inject:
            l: logger
            r: ?InfoRepository
          construct:
            logger: l
            repo: r
      controllers:
        GetInfoById:
            dto:
              InfoItemDTO:
                id: string
                content: string
            dependencies:
              logger: logger.Logger
              getInfoService: $GetInfoService
            inject:
              l: logger
              s: $GetInfoService
            construct:
              logger: l
              getInfoService: s
            params: "info-id"
            api-docs: >
              @Summary Retrieves info
              @Description Retrieves info by its id.
              @Tags Info
              @Params info-id path string true "Info's Id"
              @Success 200 {object} InfoItemDTO
              @Router /info/{info-id}
              
`````

Well, let's explain what is all this.

## Project Object
This is the main object that contains the whole configuration of 
the entire project.

Its properties are
````yaml
name: the name of the project. It will use for create the root folder
application: the declaration of the Application Object.
entities: the declaration of the Entity Objects.
````

## Application Object
It refers to the entity in charge of to be the app's root. 
In this entity are located
- the **Container** for declare the dependencies.
- the **EntryPoint** of the program.
- *Global* aspect of the program. *Such as EnvConfig, Logger, HttpClient, etc*

Its properties are
````yaml
http: Declares an application as a http server with the Application HTTP Object
console: Declares an application as a console application with the Application Console Object
````

### Application HTTP Object
This object is part of the **Application Object**, allows to create an application
as a http server.

Its properties are
````yaml
usage: Indicates what kind of HTTP Server library it will use
port: Indicates the port to run the server
base-path: Indicates the base path of the server
routes: Routes declaration by the HTTP Route Object
handlers: Handlers declaration by the HTTP Handler Object 
api-doc: Declare the General Info API Docs
````

### HTTP Route Object
This object is for declare a single Route for an Application HTTP Object.
*Inside the `routes` field the route must have unique name*.

Its properties are
````yaml
method: The HTTP verb. GET, POST, etc
path: The route path
handler: The handler name to use for this route
````

### HTTP Handler Object
This object is for declare a handler to be used for some route.
*Inside the `handlers` field the route must have unique name*.

In Golang, a handler is just a First Class Function. See **Controller Object** 
from *Entity Object* in case you need a more Object-Oriented approach.

Its properties are
````yaml
params: A list of the Path Params to use
query: A list of the Query Params to use
api-docs: The API docs for this handler
````

## Meta Caffe Object
This object is an abstract object that is used for dependency injection.
This means, the object can request dependencies for its injection.

Its properties are
````yaml
inject: A map [name]DependencyType of the dependencies to inject
dependencies: A map [name]DependencyType of the dependencies of the Object
construct: Mapper for map the injection and the dependencies of the Object
````

## Entity Object
This object is for declare a new Entity (*based on the Caffe Architecture*).

Its properties are
````yaml
domain: Declares the Entity Domain Object
repos: Repositories declaration with Entity Repository Object
services: Services declaration with Entity Service Object
controllers: Controllers declaration with Entity Controller Object
````

### Entity Domain Object
Refers to all related with the Domain of the Entity.

Its properties are
````yaml
models: Models declaration with Domain Model Object
interfaces: Interfaces declaration Domain Interface Object
errors: A map of the errors declaration. By the form [error-name]:Message
````

#### Domain Model Object
Refers to an object which its key is the field's name and the value its type

### Domain Interface Object
Refers to an object which represents an interface.
Each key is an **Interface Method Object**.

Properties of Method Object.
````yaml
docs: The method's documentation
params: A map of the Params that the method takes. By the form [name]:Type
returns: A list of the Types that the method returns.
````

### Entity Repo Object
This object is for declare a new Repository implementation. 

Its properties are
````yaml
inside-of: Indicates the sub module of the repository.
implements: Indicates which interface the repository implements.
````

### Entity Service Object
This object is for declare a new Service. *This object is fully declarative at 
the moment*.

### Entity Controller Object
This object is for declare a new Controller.

Its properties are
````yaml
dto: DTOs declaration. The key is the field's name and the value declares the type
params: A list of Path Params to use
query: A list of Query Params to use
api-docs: API Docs declaration
````